"""Stream type classes for tap-posthog."""

from typing import Optional

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_posthog.client import PosthogStream


class ProjectsStream(PosthogStream):

    name = "projects"
    path = "/projects"
    primary_keys = ["id"]
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.NumberType),
        th.Property("uuid", th.StringType),
        th.Property("organization", th.StringType),
        th.Property("name", th.CustomType({"type": ["object", "string"]})),
        th.Property("completed_snippet_onboarding", th.BooleanType),
        th.Property("ingested_event", th.BooleanType),
        th.Property("is_demo", th.BooleanType),
        th.Property("timezone", th.StringType),
        th.Property("access_control", th.BooleanType),
        th.Property("effective_membership_level", th.NumberType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        return {
            "project_id": record["id"],
        }


class CohortsStream(PosthogStream):

    name = "cohorts"
    path = "/projects/{project_id}/cohorts/"
    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = ProjectsStream
    schema = th.PropertiesList(
        th.Property("id", th.NumberType),
        th.Property("description", th.StringType),
        th.Property("name", th.CustomType({"type": ["object", "string"]})),
        th.Property("groups", th.CustomType({"type": ["array", "string"]})),
        th.Property("deleted", th.BooleanType),
        th.Property("filters", th.CustomType({"type": ["object", "string"]})),
        th.Property("is_calculating", th.BooleanType),
        th.Property("created_by", th.CustomType({"type": ["object", "string"]})),
        th.Property("created_at", th.DateTimeType),
        th.Property("last_calculation", th.DateTimeType),
        th.Property("errors_calculating", th.NumberType),
        th.Property("count", th.NumberType),
        th.Property("is_static", th.BooleanType),
        th.Property("project_id", th.NumberType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        return {
            "project_id": record["project_id"],
            "cohorts_id": record["id"],
        }


class CohortPersonsStream(PosthogStream):

    name = "cohort_persons"
    path = "/projects/{project_id}/cohorts/{cohorts_id}/persons/"
    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = CohortsStream
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("type", th.StringType),
        th.Property("uuid", th.StringType),
        th.Property("created_at", th.DateTimeType),
        th.Property("properties", th.CustomType({"type": ["object", "string"]})),
        th.Property("is_identified", th.BooleanType),
        th.Property("name", th.CustomType({"type": ["object", "string"]})),
        th.Property("distinct_ids", th.CustomType({"type": ["array", "string"]})),
        th.Property("project_id", th.NumberType),
        th.Property("cohorts_id", th.NumberType),
    ).to_dict()


class EventsStream(PosthogStream):

    name = "events"
    path = "/projects/{project_id}/events"
    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = ProjectsStream
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("distinct_id", th.StringType),
        th.Property("properties", th.CustomType({"type": ["object", "string"]})),
        th.Property("event", th.StringType),
        th.Property("timestamp", th.DateTimeType),
        th.Property("person", th.CustomType({"type": ["object", "string"]})),
        th.Property("elements", th.CustomType({"type": ["array","object", "string"]})),
        th.Property("elements_chain", th.StringType)
    ).to_dict()

class PersonsStream(PosthogStream):

    name = "persons"
    path = "/projects/{project_id}/persons"
    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = ProjectsStream
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("type", th.StringType),
        th.Property("uuid", th.StringType),
        th.Property("created_at", th.DateTimeType),
        th.Property("name", th.CustomType({"type": ["object", "string"]})),
        th.Property("is_identified", th.BooleanType),
        th.Property("properties", th.CustomType({"type": ["object", "string"]})),
        th.Property("distinct_ids", th.CustomType({"type": ["array", "string"]})),
    ).to_dict()

