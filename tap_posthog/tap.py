"""Posthog tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th  # JSON schema typing helpers

from tap_posthog.streams import (
    ProjectsStream,
    CohortsStream,
    CohortPersonsStream,
    EventsStream,
    PersonsStream
)

STREAM_TYPES = [
    ProjectsStream,
    CohortsStream,
    CohortPersonsStream,
    EventsStream,
    PersonsStream
]


class TapPosthog(Tap):
    """Posthog tap class."""

    name = "tap-posthog"
    config_jsonschema = th.PropertiesList(
        th.Property("api_key", th.StringType, required=True),
        th.Property("is_demo", th.BooleanType, required=False), # selects the demo environment base url
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapPosthog.cli()
