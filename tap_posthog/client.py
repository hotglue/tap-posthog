"""REST client handling, including PosthogStream base class."""

import requests
from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable, cast

from memoization import cached

from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream
from singer_sdk.authenticators import BearerTokenAuthenticator


class PosthogStream(RESTStream):
    """Posthog stream class."""

    _page_size = 100
    offset = 0

    records_jsonpath = "$.results[*]"
    next_page_token_jsonpath = "$.next"

    @property
    def url_base(self) -> str:
        if self.config.get('is_demo'):
            return "https://demo.posthog.com/api"
        return "https://app.posthog.com/api"

    @property
    def authenticator(self) -> BearerTokenAuthenticator:
        """Return a new authenticator object."""
        return BearerTokenAuthenticator.create_for_stream(
            self, token=self.config.get("api_key")
        )

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""

        response = response.json()
        if response.get("next") is not None:
            if response["next"] != previous_token:
                return response["next"]
        return None

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:

        params: dict = {}
        if next_page_token:
            params["offset"] = next_page_token

        params["limit"] = self._page_size
        return params

    def post_process(self, row: dict, context: Optional[dict]) -> dict:
        if 'name' in row:
            row['name'] = str(row['name'])
        if context is not None:
            if "project_id" in context:
                row["project_id"] = context["project_id"]
            if "cohorts_id" in context:
                row["cohorts_id"] = context["cohorts_id"]
        return row

    def prepare_request(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> requests.PreparedRequest:

        http_method = self.rest_method

        if next_page_token is not None:
            url: str = next_page_token
        url: str = self.get_url(context)

        params: dict = self.get_url_params(context, {})
        request_data = self.prepare_request_payload(context, {})
        headers = self.http_headers

        authenticator = self.authenticator
        if authenticator:
            headers.update(authenticator.auth_headers or {})
            params.update(authenticator.auth_params or {})

        request = cast(
            requests.PreparedRequest,
            self.requests_session.prepare_request(
                requests.Request(
                    method=http_method,
                    url=url,
                    params=params,
                    headers=headers,
                    json=request_data,
                ),
            ),
        )
        return request
